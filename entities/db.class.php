<?php

/*
SQL USED.

CREATE DATABASE testFile;

USE testFile;
CREATE TABLE DailyStats(
    -> Id INT(4) NOT NULL AUTO_INCREMENT,
    -> LastUpdate DATETIME,
    -> HitCount INT(11),
    -> LastTag VARCHAR(32),
    -> PRIMARY KEY (ID)

);

SHOW TABLE COLSSS

DESC DailyStats;
*/

namespace Test\Entities;
use mysqli;
class dbLayer {


	const DB = 'testFile';
	const SERVER = 'localhost';
	const USER = 'root';
	const PWD = 'nubi';
	const TBL = 'DailyStats';

	protected $conn;

	public  function __construct() {

		try {
			$conn = new mysqli(self::SERVER, self::USER, self::PWD);
			$this->conn = $conn;
			mysqli_select_db($this->conn, self::DB);

		}
		catch(PDOException $e)
	    {
	    	echo "Connection failed: " . $e->getMessage();
	    }	   

	}


	/*
	* Method to save or update a row.
	*/
	public function customQuery($query, $rowValues, $isDate) {

		// Create table DailyStats if not exists yet.
		$this->createTable();

		try {	
			// query is a MYSQLI method.
			$rows = $this->conn->query($query);
			$totalRows = $rows->fetch_object();
			 
		}catch(PDOException $e)
	    {
	    	return "Connection db failed: " . $e->getMessage();

	    }    


	    // If daily row not exits for this day, we create a new one.
	    if ( $totalRows->total < 1 ) {

	    	$datetimeLastUpdate = date('Y/m/d H:i:s', $rowValues['LastUpdate']);
	    	$sql = "INSERT INTO " .self::TBL. "(LastUpdate, HitCount, LastTag) 
	    	VALUES ('" .$datetimeLastUpdate. "', '" .$rowValues['HitCount']. "', '" .$rowValues['LastTag']. "')";


			if ($this->conn->query($sql) === TRUE) {

			    return "New record created successfully date: $isDate)";
			} else {
				var_dump( $this->conn->error);
			    return "Error: " . $sql . "<br>" . $this->conn->error;
			}
	    }else{
	    	// Update
	    	$sql = "UPDATE " .self::TBL. " SET HitCount='" .$rowValues['HitCount']. "', LastTag=' ".$rowValues['LastTag']." '  
	    	 $isDate ";
	    	if ($this->conn->query($sql) === TRUE) {
	    		
			    return "Record  was updated created successfully (date: $isDate)";
			} else {
			    return "Error: " . $sql . "<br>" . $this->conn->error;
			}

	    }	
	}

	public function rows($query){
	
		// query is a MYSQLI method.
		$result = $this->conn->query($query);
	
		$rows = $this->fetchRows($result);

		return $rows;
	}

	private function fetchRows($result) {

		while ($row = $result->fetch_object()){
        	$rows[] = $row;
    	}

    	return ( isset($rows) ) ? $rows : '';
	}

	// Create table if not exists yet.
	private function createTable() {


		$query = "SELECT Id FROM " .self::TBL;
		$result =  $this->conn->query( $query );

		if(empty($result)) {
			try {	
		        $query = "CREATE TABLE " .self::TBL. " (
		                  Id int(4) AUTO_INCREMENT,
		                  LastUpdate DATETIME,
		                  HitCount int(11),
		                  LastTag VARCHAR(32),
		                  PRIMARY KEY  (Id)
		                  )";
		        $result = $this->conn->query( $query );
	    	}catch(PDOException $e)
	    	{
	    		return "Connection db failed: " . $e->getMessage();

	    	}  
    	}

    	return;
	}

}
