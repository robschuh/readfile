<?php

/*
Helper class to handle different file tasks.
*/
namespace Test\Entities;

use Test\Helpers\FileHelper;

class BinaryFile {

	private $fileName;
	private $lines = 5; // Create five lines by default.

	public function __construct($name, $lines) {
		$this->fileName = $name;
		$this->lines = $lines;

		$this->createBinary();
	}
	private function createBinary() {

		$myfile = fopen($this->fileName, "w") or die("Unable to open file!");
		$txt = '';
		for ( $i = 0; $i <= $this->lines; $i++ ) {
			
			// Generate a string with 32 bits (0-3).
			$txt .= rand (1000000000, 2147483647);

			// Generate a number with 32 bits (4-7).
			$txt .= rand (1000000000, 2147483647);

			// Generate a string with 24 caracters.
			$txt .= FileHelper::getRanbdomString(24);
			fwrite($myfile, $txt);

		}
		fclose($myfile);
	}

	public function getBinaryB64File(BinaryFile $binaryFile) {
		
		$file = fopen($binaryFile->fileName, 'r');
		if ($file) {
		    
		    $str = stream_get_contents($file, -1, 0);	   
		    fclose($file);
		}
		
		return base64_encode($str);		
	}

	public function getDecodeString($strEncode) {
		
		$strEncode = str_replace(' ','+',$strEncode);
		return base64_decode($strEncode);	
	}

}
