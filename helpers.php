<?php

namespace Test\Helpers;

class FileHelper {

	// Create a randon string with $length caracters.
	public static function getRanbdomString ($length) {

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength = strlen($characters);
    	$randomString = '';
    	

    	// Get a 24 characters.
		for ($i = 0; $i < $length ; $i++) {
	    	$randomString .= $characters[rand(0, $charactersLength - 1)];

		}

		return $randomString;
	}

}
