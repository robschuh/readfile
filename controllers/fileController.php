<?php

/*
File main controller.
*/
namespace Test\Controllers;

use Test\Entities\binaryFile;
use Test\Entities\dbLayer;
use Test\Helpers\FileHelper;

use random_bytes;
class FileController {
	

	// Get all the file information row by row.
	public function index(BinaryFile $binaryFile) {
		

		 $file = $binaryFile->getBinaryB64File($binaryFile);

		 // Extract file rows.
		 $fileRows = $binaryFile->getDecodeString($file);



		 $i = 0;
		 $fileLength = strlen($fileRows);


		 while( $i < $fileLength ){
		 	$rows[] = substr($fileRows, $i, 44);
		 	$i = $i +44;
		 }

		 return $rows;
	}


	/*
	* Test action to create a demo file with random information.
	*/
	public function create ($name, $lines) {
		 $binaryFile = new BinaryFile($name, $lines);

		 return  $binaryFile;
	}

	// Store file rows in database.
	public function store($fileRows) {

		$row = array();

		// Get the Lastupdate (32 bits) from the file.
	 	$row['LastUpdate'] = substr($fileRows, 0,  10);

	 	// Get the HitCount (32 bits) from the file.
	 	$row['HitCount'] = substr($fileRows, 10, 10);

	 	// Get the LastTag (24 characters) from the file.
	 	$row['LastTag'] = substr($fileRows, 11, 23).'\0';
	 
	 	// Save row in our database.
		return $this->saveRow($row);	
				
	}

	/*
	* Save or update a row.
	*/
	private function saveRow(array $rowValues) {

		// Check if date already exists in our db.
		$db = new dbLayer();
		$cols = array();

		$datetimeLastUpdate = date('Y/m/d', $rowValues['LastUpdate']);

		$isDate = "WHERE DATE(LastUpdate) = '" .$datetimeLastUpdate. "' ";
		$query = "SELECT COUNT(*) as total FROM DailyStats $isDate";

		return $db->customQuery($query, $rowValues, $isDate);
				
	}

	// Get all the rorws stores in DailyStats table (test purpose).
	public function getRows() {

		$db = new dbLayer();
		$query = "SELECT *  FROM DailyStats ORDER BY LastUpdate";
		$rows = $db->rows($query);

		return $rows;
	}
}
