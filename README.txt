

This code has been build only for a test purpose.

To run it please you need to create db named "testFile" and call the main controller index.php by POST, you have to send the currentDate PARAM (by post).
ejm http://localhost/binary/index.php (method: POST)

NOTE: normally I use a FW like laravel to do this common tasks, and composer to autoload classes, but for this exercise I did not use any of them.

Results structure by JSON (exm):
[
	{"Id":"4","LastUpdate":"2004-09-03 16:07:48","HitCount":"1261997184","LastTag":" 261997184XJXYSHjey7zmZQ\u0000"},
	{"Id":"7","LastUpdate":"2005-06-05 07:13:58","HitCount":"2124881773","LastTag":" 124881773FbYWXSyHio1uCR\u0000"},
	{"Id":"8","LastUpdate":"2012-10-05 22:18:44","HitCount":"1158102586","LastTag":" 1581025863TS9SxkQuiJ201\u0000"},
	{"Id":"6","LastUpdate":"2014-06-11 07:32:15","HitCount":"1212544050","LastTag":"212544050o4kO0aVC5hbKAJ\u0000"},
	{"Id":"5","LastUpdate":"2016-12-0703:05:32","HitCount":"2072613194","LastTag":" 072613194Q7BVlzEjxx0RLz\u0000 "},
	{"Id":"2","LastUpdate":"2017-03-29 08:17:30","HitCount":"1824211638","LastTag":" 824211638qMVoLkKjoACBU6\u0000"},
	{"Id":"9","LastUpdate":"2023-10-20 01:40:22","HitCount":"1192252439","LastTag":"192252439L2b4beX3oPBJG5\u0000"},
	{"Id":"3","LastUpdate":"2031-07-26 01:04:13","HitCount":"1292455817","LastTag":"292455817khiLZIxV7jfRDD\u0000"},
	{"Id":"1","LastUpdate":"2037-05-23 03:38:53","HitCount":"2132502455","LastTag":" 132502455olx4Qidju13nIc\u0000 "}
]