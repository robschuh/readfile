<?php

// MAIN CONTROLLER to handle all the APP request.
// NOTE each time we run this MAIN controller it generate a new file (and it reset the old one).
// NOTE this main controller HAS NOT html code, not header , not body.

require('entities/db.class.php');
require('entities/file.class.php');
require('controllers/fileController.php');
require('helpers.php');

use Test\Entities\binaryFile;
use Test\Controllers\fileController;

// Load our file controller.
$fileController = new fileController();

// Generate a file with random information (test propousal).
$binaryFileObj = $fileController->create('testFile.txt', 2);


// POST method, returns a json with all the rows data.
if(isset($_POST['currentDate'])) {

	$fileRows = $fileController->index($binaryFileObj);

	header('Content-Type: application/json');
		
	// Save file information in our db,
	foreach ($fileRows as $row){
		$resultsQueries['data'][] = $fileController->store($row);
		
	}
	
	$data = $fileController->getRows();

	echo json_encode( $data );
}else {
	 echo json_encode( 'method not allowed, please run this example with POST, thanks' );
}

/*
	// TEST PURPOSE
	$data = $fileController->getRows();

	// Load main Logs view (only for test purpose), without DOM elements.
	include 'view/logs.html.php';
*/
